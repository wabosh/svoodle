export interface Choice {
	option: string | Date;
}

export interface Answer {
	choice: Choice;
	value: 'yes' | 'no' | 'maybe';
}
